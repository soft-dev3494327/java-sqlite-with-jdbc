/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nalinthip.databaseproject;

import com.nalinthip.databaseproject.dao.UserDao;
import com.nalinthip.databaseproject.helper.DatabaseHelper;
import com.nalinthip.databaseproject.model.User;

/**
 *
 * @author ACER
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        // getAll
        System.out.println("getAll");
        for (User u : userDao.getAll()) {         
            System.out.println(u);
        }
        // get
//        User user1 = userDao.get(2);
//        System.out.println("get");
//        System.out.println(user1);

        // save
//        System.out.println("save");
//        User newUser = new User("user3", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
        
        // update
//        System.out.println("update");
//        insertedUser.setGender("M");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
        
        // delete
//        System.out.println("delete");
//        userDao.delete(user1);
//        for (User u : userDao.getAll()) {         
//            System.out.println(u);
//        }
        
        // getAll
        System.out.println("getAll");
        for (User u : userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {         
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
